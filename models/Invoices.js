const path = require('path');
const xml2js = require('xml2js');
const fs = require('fs');
const parser = new xml2js.Parser({ ignoreAttrs: false, mergeAttrs: true });

function invoices() {
    const directoryPath = path.join(__dirname, 'documents');
    let res = []
    try {
        let data = fs.readdirSync(directoryPath)
        data.forEach((invoiceName) => {
            let xml_string = fs.readFileSync(path.join(__dirname, 'documents', invoiceName), "utf8");
            parser.parseString(xml_string, function (error, result) {
                if (error === null) {
                    res.push(result)
                }
                else {
                    console.log(error);
                }
            });
        })
        //se ordena según numero de emisión, ya que fecha no está disponible en el xml
        res.sort(function (a, b) {
            if (a.dte.emision < b.dte.emision)
                return -1
            if (a.dte.emision > b.dte.emision)
                return 1
            return 0
        })
        return res
    }
    catch (error) {
        return null
    }

}

module.exports = { invoices }