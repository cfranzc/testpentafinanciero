const invoices = require('../models/Invoices')

module.exports = (app) => {

  app.get(`/api/invoices`, async (req, res) => {
    let data = await invoices.invoices();
    if(data==null)
      return res.status(500).send({error:"No existe carpeta de archivos"})
    else
      return res.status(200).send(data);
  });
}