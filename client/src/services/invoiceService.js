import axios from 'axios'

export default {
    getInvoices: async () => {
        try {
            let data = await axios.get('/api/invoices')
            return data || []
        }catch(error){
            return {data : []}
        }  
    }
}