import React, { useState, useEffect } from 'react';
import './App.css';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Container from '@material-ui/core/Container';

import invoiceService from './services/invoiceService'

const useStyles = makeStyles(theme => ({
  root: {
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  header: {
    padding: theme.spacing(2),
  },
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  appBarSpacer: theme.mixins.toolbar,
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
}));

function App() {
  const classes = useStyles();
  const [invoices, setInvoices] = useState(null)

  useEffect(() => {
    if (!invoices) {
      getInvoices()
    }
  })

  const getInvoices = async () => {
    let res = await invoiceService.getInvoices()
    setInvoices(res)
  }

  const renderInvoice = invoice => {
    console.log(invoice)
    ///////////////////////////////////////////
    //emision como key, pensando que es unico//
    return (
      <TableRow key={invoice.dte.emision}>
        <TableCell>{invoice.dte.emision}</TableCell>
        <TableCell>{invoice.dte.tipo}</TableCell>
        <TableCell>{invoice.dte.folio}</TableCell>
        <TableCell>{invoice.dte.emisor[0].rut}</TableCell>
        <TableCell>{invoice.dte.emisor[0].razonSocial}</TableCell>
        <TableCell>{invoice.dte.receptor[0].rut}</TableCell>
        <TableCell>{invoice.dte.receptor[0].razonSocial}</TableCell>
        <TableCell>{
          invoice.dte.items[0].detalle.length > 0 ? (
            <ul>{
              invoice.dte.items[0].detalle.map((item, index) => (
                <li key={index}>{item._}, Valor: {item.monto}, Iva: {item.iva}</li>
              ))
            }</ul>
          ) : (
              <ul>
                <li>Sin items</li>
              </ul>
            )
        }</TableCell>
      </TableRow>
    )
  }
  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="absolute" className={classes.appBar}>
        <Toolbar className={classes.toolbar}>
          <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
            Lista de Facturas
          </Typography>
        </Toolbar>
      </AppBar>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="lg" className={classes.container}>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <Table size="small">
                <TableHead>
                  <TableRow>
                    <TableCell>Emisión</TableCell>
                    <TableCell>Tipo</TableCell>
                    <TableCell>Folio</TableCell>
                    <TableCell>Rut emisor</TableCell>
                    <TableCell>Razón Social emisor</TableCell>
                    <TableCell>Rut receptor</TableCell>
                    <TableCell>Razón Social receptor</TableCell>
                    <TableCell>Items</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {(invoices && invoices.data.length > 0) ? (
                    invoices.data.map(invoice => renderInvoice(invoice))
                  ) : (
                      <TableRow>
                        <TableCell>Son facturas</TableCell>
                        <TableCell></TableCell>
                        <TableCell></TableCell>
                        <TableCell></TableCell>
                        <TableCell></TableCell>
                        <TableCell></TableCell>
                        <TableCell></TableCell>
                        <TableCell></TableCell>
                      </TableRow>
                    )}
                </TableBody>
              </Table>
            </Paper>
          </Grid>
        </Container>
      </main>
    </div >
  );
}

export default App;
