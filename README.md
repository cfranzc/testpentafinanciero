**Test para Penta Financiero**

Este proyecto esta desarrollado con Node.js en el backend y React JS para el frontend, los archivos se leen en el backend y se guardan en un objeto para luego desplegarlos en la vista, el requerimiento pedía ordenar por fecha, pero el contenido no tiene este campo, por lo cual lo ordené por el número de emisión, (se puede cambiar en caso de que se incluya la fecha en los documentos).


---

## Como instalar y ejecutar la aplicación

Para instalar esta aplicación, anteriormente debes tener instalado node en tu equipo.

1. Ir a la carpeta raiz del proyecto y ejecutar *npm install*.
2. Luego ingresar a la carpeta /client y ejecutar *npm install*.
3. Volver a la carpeta raiz /.
4. Ejecutar *npm run dev* para arrancar la aplicación
5. Si no se abre automáticamente, ve a tu navegador ingresando  **http://localhost:3000/**

---